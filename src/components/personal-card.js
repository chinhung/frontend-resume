import React from 'react';
import Image from './image';

import oneStatement from './one-statemant';

const PersonalCard = () => (
  <>
    <div className="w3-white w3-text-grey w3-card-4">

      <div className="w3-display-container">
        {/* <Image /> */}
        <img className="w3-circle" style={{width: '100%', padding: '20%'}} src="https://lh3.google.com/u/0/d/1hgxRH873Uhtf3zm3MjylPtbP3PlDKdy_=w2880-h1576-iv1"></img>
        <div className="w3-display-bottomleft w3-container w3-text-black">
          <h2>陳錦宏</h2>
        </div>
      </div>

      <div className="w3-container">
        <h5>{oneStatement}</h5>
        <hr/>
      </div>

      <div className="w3-container">
        <p><i className="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>軟體工程師</p>
        <p><i className="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>新北市</p>
        <p><i className="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i>chinhung101@gmail.com</p>
        <p><i className="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i>0932869706</p>
        <hr/>

        <p className="w3-large"><b><i className="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i>關於我</b></p>
        <p>我擁有穩定的產出能力以及寬廣的技術視野。前3年的職涯專注於新產品與新功能的開發，養成了靈活的游擊隊作戰能力。雖然游擊隊的作戰能力有助於探索各種技術與商業的可能性，但在試圖創造價值正向循環的過程中感到力不從心。我理解到自己的下一個成長里程碑，是培養自己正規軍的作戰能力。</p>
        <hr/>

        <p className="w3-large"><b><i className="fa fa-gitlab fa-fw w3-margin-right w3-text-teal"></i>GitLab Repositories</b></p>
        <p><a target="_blank" href="https://gitlab.com/chinhung/frontend-resume">This Site</a></p>

      </div>

    </div>
  </>
)

export default PersonalCard;